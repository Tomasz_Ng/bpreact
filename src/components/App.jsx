import React, {Component, Fragment} from 'react';
import Header from './includes/Header';
import Footer from './includes/Footer';
import Home from './pages/Home';

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <main>
                    <Home/>
                </main>
                <Footer/>
            </Fragment>
        );
    }
}
export default App;