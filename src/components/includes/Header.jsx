import React, {Component} from 'react';

class Header extends Component {
    render() {
        return (
            <header>
                <nav>
                    <ul>
                        <li>home</li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;