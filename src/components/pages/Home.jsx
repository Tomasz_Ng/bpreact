import React, {Component} from 'react';

import image from '../../images/background.jpg';

class Home extends Component {
    render() {
        return (
            <section>
                <h1>Home page</h1>
                <img src={image}/>
            </section>
        );
    }
}

export default Home;