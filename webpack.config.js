const dotenv = require('dotenv').config();
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = (env, opts) => {
    // webpack local vars
    env = Object.keys(dotenv.parsed).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(dotenv.parsed[next]);
        return prev;
    }, {});

    let envMode = opts.mode !== 'production';

    return {
        // Webpack configs
        entry: path.join(__dirname, '/src/index.js'),
        output: {
            path: path.join(__dirname, '/dist'),
            filename: envMode ? 'js/[name].min.js' : 'js/[name].[chunkhash].min.js',
            publicPath: '/'
        },
        devServer: {
            contentBase: path.join(__dirname, '/dist'),
            compress: true,
            port: 9000,
            disableHostCheck: true
        },
        plugins: [
            new webpack.DefinePlugin(env),
            new HtmlWebpackPlugin({
                template: path.join(__dirname, '/src/index.html'),
                filename: envMode ? 'index.html' : 'index.[hash].html',
                minify: {
                    collapseWhitespace: true
                },
                title: process.env.APP_NAME,
                meta: {
                    viewport: 'width=device-width, initial-scale=1.0, shrink-to-fit=no'
                },
                favicon: './src/favicon.ico'
            }),
            new MiniCssExtractPlugin({
                filename: envMode ? 'css/[name].min.css' : 'css/[name].[hash].min.css'
            }),
            new OptimizeCSSAssetsPlugin(),
            new UglifyJsPlugin({
                extractComments: {
                    condition: true,
                    filename(file) {
                        return `${file}.LICENSE`;
                    },
                    banner(licenseFile) {
                        return `License information can be found in ${licenseFile}`;
                    }
                }
            })
        ],
        resolve: {
            extensions: ['*', '.js', '.jsx']
        },

        // Webpack modules
        module: {
            rules: [
                {
                    test: /\.js|jsx$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    query: {
                        presets:['@babel/env','@babel/react']
                    }
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    loader: 'file-loader',
                    options: {
                        name: envMode ? 'images/[name].[ext]' : 'images/[hash].[ext]'
                    }
                },
                {
                    test: /\.(woff(2)?|ttf|eot)$/,
                    loader: 'file-loader',
                    options: {
                        name: envMode ? 'fonts/[name].[ext]' : 'fonts/[hash].[ext]'
                    }
                }
            ]
        }
    };
};